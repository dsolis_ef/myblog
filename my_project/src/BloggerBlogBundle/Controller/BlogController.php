<?php
// src/Blogger/BlogBundle/Controller/BlogController.php

namespace BloggerBlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controlador del Blog.
 */
class BlogController extends Controller
{
    /**
     * Muestra una entrada del blog
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find($id);

        if (!$blog) {
            throw $this->createNotFoundException('No se puede encontrar el Blog solicitado.'.$id);
        }

        $comments = $em->getRepository('BloggerBlogBundle:Comment')
                   ->getCommentsForBlog($blog->getId());


        $respuesta = new Response();

        $respuesta= $this->render('BloggerBlogBundle:Blog:show.html.twig', array(
            'blog'      => $blog,
            'comments'  => $comments
        ));

       $respuesta->setMaxAge(60);
    $respuesta->setSharedMaxAge(60);

    return $respuesta;


    }

    public function EsiAction($maxPerPage){

       //var_dump("expression");

        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find(19);

        if (!$blog) {
            throw $this->createNotFoundException('No se puede encontrar el Blog solicitado.'.$id);
        }

        $response = new Response();

         $response= $this->render('BloggerBlogBundle:Blog:esi.html.twig', array(
            'blog'      => $blog
        ));

        $response->setSharedMaxAge(10);

        return $response;



    }
}