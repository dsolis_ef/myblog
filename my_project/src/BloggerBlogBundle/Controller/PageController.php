<?php

namespace BloggerBlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use BloggerBlogBundle\Entity\Enquiry;
use BloggerBlogBundle\Form\EnquiryType;

use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
	 public function indexAction(Request $request)
    {

    	$searchQuery = $request->get('query');

    	if(!empty($searchQuery))
    	{
    		
    		$finder = $this->container->get('fos_elastica.finder.appdsg.blog');
    		$blogs = $finder->find($searchQuery); 
		
    		
    	}
    	else
    	{
    		$em = $this->getDoctrine()->getManager();

        	$blogs = $em->getRepository('BloggerBlogBundle:Blog')
                    ->getLatestBlogs();
    	}


    	
    	


    	

        return $this->render('BloggerBlogBundle:Page:index.html.twig', array(
            'blogs' => $blogs
        ));
    }

    public function es_AggAction(){

          /*$query = new \Elastica\Query(new \Elastica\Query\MatchAll());

        // Simple aggregation (based on tags, we get the doc_count for each tag)
        $tagsAggregation = new \Elastica\Aggregation\Terms('tag');
        $tagsAggregation->setField('tags');

        $query->addAggregation($tagsAggregation);

        // we don't need the search results, only statistics
        $query->setSize(0);
       */


        $finder = $this->container->get('fos_elastica.finder.appdsg.blog');
        
         $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('BloggerBlogBundle:Blog')->getStatsQuery();

        $blogs = $finder->search($query);
        
        var_dump($blogs); 
        
        /* Con esto si funciona:
        $finder = $this->container->get('fos_elastica.finder.appdsg.blog');
        $blogs = $finder->find("symblog"); */
            

        return $this->render('BloggerBlogBundle:Page:es_agg.html.twig', array(
            'blogs' => $blogs
        ));
    }

    public function aboutAction()
    {
        return $this->render('BloggerBlogBundle:Page:about.html.twig');
    }
    public function contactAction()
	{
	     $enquiry = new Enquiry();

	     

    	//$form = $this->createForm(EnquiryType::class, $enquiry);

	    
	     
	     $form = $this->createForm(new EnquiryType(), $enquiry);



	    $request = $this->getRequest();
	    if ($request->getMethod() == 'POST') {
	        $form->handleRequest($request);

	        if ($form->isValid()) {
	             $message = \Swift_Message::newInstance()
			            ->setSubject('Contact enquiry from symblog')
			            ->setFrom('devsyst2000@gmail.com')
			            ->setTo($this->container->getParameter('blogger_blog.emails.contact_email'))
			            ->setBody($this->renderView('BloggerBlogBundle:Page:contact.txt.twig', array('enquiry' => $enquiry)));
				$this->get('mailer')->send($message);

		        // $this->get('session')->setFlash('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');

		        $this->get('session')->getFlashBag()->set('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');


		        // Redirige - Esto es importante para prevenir que el usuario
		        // reenvíe el formulario si actualiza la página
        		return $this->redirect($this->generateUrl('BloggerBlogBundle_contact'));
	        }
	    }

	    return $this->render('BloggerBlogBundle:Page:contact.html.twig', array(
	        'form' => $form->createView()
	    ));
	}
}
