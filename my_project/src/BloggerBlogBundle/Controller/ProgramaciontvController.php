<?php

namespace BloggerBlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BloggerBlogBundle\Entity\Programaciontv;
use BloggerBlogBundle\Form\ProgramaciontvType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Programaciontv controller.
 *
 * @Route("/programaciontv")
 */
class ProgramaciontvController extends Controller
{
    /**
     * Lists all Programaciontv entities.
     *
     * @Route("/", name="programaciontv_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $programaciontvs = $em->getRepository('BloggerBlogBundle:Programaciontv')->findAll();


        return $this->render('programaciontv/index.html.twig', array(
            'programaciontvs' => $programaciontvs,
        ));
    }

    /**
     * Creates a new Programaciontv entity.
     *
     * @Route("/new", name="programaciontv_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $programaciontv = new Programaciontv();
        $form = $this->createForm('BloggerBlogBundle\Form\ProgramaciontvType', $programaciontv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($programaciontv);
            $em->flush();

            return $this->redirectToRoute('programaciontv_show', array('id' => $programaciontv->getId()));
        }

        return $this->render('programaciontv/new.html.twig', array(
            'programaciontv' => $programaciontv,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Programaciontv entity.
     *
     * @Route("/{id}", name="programaciontv_show")
     * @Method("GET")
     */
    public function showAction(Programaciontv $programaciontv)
    {
        $deleteForm = $this->createDeleteForm($programaciontv);

        return $this->render('programaciontv/show.html.twig', array(
            'programaciontv' => $programaciontv,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Programaciontv entity.
     *
     * @Route("/{id}/edit", name="programaciontv_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Programaciontv $programaciontv)
    {
        $deleteForm = $this->createDeleteForm($programaciontv);
        $editForm = $this->createForm('BloggerBlogBundle\Form\ProgramaciontvType', $programaciontv);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($programaciontv);
            $em->flush();

            return $this->redirectToRoute('programaciontv_edit', array('id' => $programaciontv->getId()));
        }

        return $this->render('programaciontv/edit.html.twig', array(
            'programaciontv' => $programaciontv,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Programaciontv entity.
     *
     * @Route("/{id}", name="programaciontv_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Programaciontv $programaciontv)
    {
        $form = $this->createDeleteForm($programaciontv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($programaciontv);
            $em->flush();
        }

        return $this->redirectToRoute('programaciontv_index');
    }

    /**
     * Creates a form to delete a Programaciontv entity.
     *
     * @param Programaciontv $programaciontv The Programaciontv entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Programaciontv $programaciontv)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('programaciontv_delete', array('id' => $programaciontv->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    public function getschedulesAction(){
        $em = $this->getDoctrine()->getManager();


        $programaciontvs = $em->getRepository('BloggerBlogBundle:Programaciontv')->findAll();




$response = array();
    foreach ($programaciontvs as $schedule) {
        $response[] = array(
            'id' => $schedule->getId(),
            'desde' => $schedule->getdesde()->format('H:i'),
            'hasta' => $schedule->gethasta()->format('H:i'),
            'programa' => $schedule->getprograma(),
            'dia' => $schedule->getdia()

            // other fields
        );
    }

    return new JsonResponse($response);

               /*$response = new Response();

$response->setContent(json_encode(array(
            'success' => TRUE,
            'schedules' => json_encode($programaciontvs)
            
        )));


        $response->headers->set('Content-Type', 'application/json');
        return $response;*/


    }

    public function gridAction()
    {
        $em = $this->getDoctrine()->getManager();

        $programaciontvs = $em->getRepository('BloggerBlogBundle:Programaciontv')->findAll();


        return $this->render('programaciontv/grid.html.twig', array(
            'programaciontvs' => $programaciontvs,
        ));
    }

    public function updateschedulesAction($id, Request $request)
    {

        $put_str = $this->getRequest()->getContent();

        $request = $this->getRequest();
        $data= json_decode($request->getContent());

        //var_dump($data);

        //var_dump($data->programa);

        //NULL: var_dump($request->request->get('id'));


        $em = $this->getDoctrine()->getManager();

        $schedule = $em->getRepository('BloggerBlogBundle:Programaciontv')->find($data->id);

        
        $hdesde = \DateTime::createFromFormat('H:i', $data->desde);

        $hhasta = \DateTime::createFromFormat('H:i', $data->hasta);

        $schedule->setDesde($hdesde);
        $schedule->setHasta($hhasta);

        $schedule->setPrograma($data->programa);
        $schedule->setdia($data->dia);

        $em->persist($schedule);
        $em->flush();



        return $this->render('programaciontv/grid.html.twig', array(
            'programaciontvs' => $programaciontvs,
        ));
    }


}
