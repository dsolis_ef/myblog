<?php

namespace BloggerBlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function helloAction($name)
    {
        return $this->render('BloggerBlogBundle:Default:hello.html.twig', array('name' => $name));
    }
    public function indexAction()
    {
        return $this->render('BloggerBlogBundle:Default:index.html.twig');
    }
}
