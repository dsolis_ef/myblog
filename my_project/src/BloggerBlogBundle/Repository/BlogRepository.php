<?php
// src/Blogger/BlogBundle/Repository/BlogRepository.php

namespace BloggerBlogBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * BlogRepository
 *
 * Esta clase fue generada por el ORM de Doctrine. (No es cierto, yo (DSG) la tuve que generar manualmente, al parecer porque ya ahbia creado antes la entidad blog) Abajo añade
 * tu propia personalización a los métodos del repositorio.
 */
class BlogRepository extends EntityRepository
{

	public function getLatestBlogs($limit = null)
    {
        $qb = $this->createQueryBuilder('b')
                   ->select('b')
                   ->addOrderBy('b.created', 'DESC');

        if (false === is_null($limit))
            $qb->setMaxResults($limit);

        return $qb->getQuery()
                  ->getResult();
    }

    public function getStatsQuery()
    {
        $query = new \Elastica\Query(new \Elastica\Query\MatchAll());

        // Simple aggregation (based on tags, we get the doc_count for each tag)
        $tagsAggregation = new \Elastica\Aggregation\Terms('Perrito_tags');
        $tagsAggregation->setField('tags');

        $query->addAggregation($tagsAggregation);

        // we don't need the search results, only statistics
        $query->setSize(0);

        return $query;
    }
    

}