### Limpiando cache de symfony  ###

sudo rm -R app/cache/*

### Cache de symfony  ###

En web/app.php , descomentar: $kernel = new AppCache($kernel);

luego habilitar en ap/AppCache.php el siguiente metodo:

 {
        return array(
        	'debug' => true,
        	'default_ttl'=> 0,
        	'private_headers'=>array('Authorization', 'Cookie'),
        	'allow_reload' => false,
        	'allow_revalidate' => false,
        	'stale_while_revalidate'=>2,
        	'stale_if_error'=>60,);

	}

donde debug cuando es tru , incluye una cabeceracon el msg de error del cache de symfony

Ejemplo de metodo para hacer uso de la cache de symfony :

 public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find($id);

        if (!$blog) {
            throw $this->createNotFoundException('No se puede encontrar el Blog solicitado.'.$id);
        }

        $comments = $em->getRepository('BloggerBlogBundle:Comment')
                   ->getCommentsForBlog($blog->getId());


        $respuesta = new Response();

        $respuesta= $this->render('BloggerBlogBundle:Blog:show.html.twig', array(
            'blog'      => $blog,
            'comments'  => $comments
        ));

       $respuesta->setMaxAge(60);
    $respuesta->setSharedMaxAge(60);

    return $respuesta;


    }



### ESI ###

Ya está preparado para funcionar con la cache de symfony y con ESI

Se configura desde  

app/config/config.yml

Con los siguientes parametros:

framework:
    #esi:             ~
    esi: { enabled: true }
    fragments: { path: /_fragment }


* Y desde la vista base se incluye:

{{ render_esi(controller('BloggerBlogBundle:Blog:Esi', { 'maxPerPage': 5 })) }}

* Y desde el controlador está el siguiente ejemplo:

 public function EsiAction($maxPerPage){

       //var_dump("expression");

        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find(19);

        if (!$blog) {
            throw $this->createNotFoundException('No se puede encontrar el Blog solicitado.'.$id);
        }

        $response = new Response();

         $response= $this->render('BloggerBlogBundle:Blog:esi.html.twig', array(
            'blog'      => $blog
        ));

        $response->setSharedMaxAge(10);

        return $response;



    }