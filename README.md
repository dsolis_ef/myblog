### ESI ###

Ya está preparado para funcionar con la cache de symfony y con ESI

Se configura desde  

app/config/config.yml

Con los siguientes parametros:

framework:
    #esi:             ~
    esi: { enabled: true }
    fragments: { path: /_fragment }


Y desde la vista base se incluye:

{{ render_esi(controller('BloggerBlogBundle:Blog:Esi', { 'maxPerPage': 5 })) }}

Y desde el controlador está el siguiente ejemplo:

 public function EsiAction($maxPerPage){

       //var_dump("expression");

        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find(19);

        if (!$blog) {
            throw $this->createNotFoundException('No se puede encontrar el Blog solicitado.'.$id);
        }

        $response = new Response();

         $response= $this->render('BloggerBlogBundle:Blog:esi.html.twig', array(
            'blog'      => $blog
        ));

        $response->setSharedMaxAge(10);

        return $response;



    }